<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
  DB::table('product')->insert(
                        array(
								array( 'product_name' => 'laptop',
								'quantity_stock' => 10,
								'price' => 100,
								'created_at' => new DateTime),

								array('product_name' => 'Load Sensor Amplifier',
								'quantity_stock' => 20,
								'price' => 200,
								'created_at' => new DateTime),

								array( 'product_name' => 'LED Based Stop Watch',
								'quantity_stock' => 15,
								'price' => 20,
								'created_at' => new DateTime),   

								array('product_name' => 'Multi Sound Buzzer',
								'quantity_stock' => 300,
								'price' => 50,
								'created_at' => new DateTime),

								array('product_name' => 'Smart DC fan',
								'quantity_stock' => 100,
								'price' => 1500,
								'created_at' => new DateTime),  

                                                           
                        ));
    }
}



